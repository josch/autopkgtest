#!/bin/sh
# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-or-later

set -eu

if dpkg-vendor --is ubuntu; then
    vendor=ubuntu
    suite=$(ubuntu-distro-info --latest)
else
    # default to Debian
    vendor=debian
    suite=testing
fi

# Make docker inherit the proxy configuration from the environment.
# This only supports systemd for now. Newer Docker version (>= 23)
# support configuring the Docker daemon proxy via a daemon.json
# configuration file, see [1], however at the moment Debian unstable
# packages Docker 20.
#
# [1] https://docs.docker.com/config/daemon/systemd/#httphttps-proxy
if [ -n "${http_proxy:-}${https_proxy:-}" ] && [ -d /run/systemd/system ]; then
    proxy_env_conf=/etc/systemd/system/docker.service.d/proxy-config.conf
    mkdir -p "$(dirname $proxy_env_conf)"
    echo "[Service]" > $proxy_env_conf
    [ -n "${http_proxy:-}" ] && echo "Environment='HTTP_PROXY=$http_proxy'" >> $proxy_env_conf
    [ -n "${https_proxy:-}" ] && echo "Environment='HTTPS_PROXY=$https_proxy'" >> $proxy_env_conf
    [ -n "${no_proxy:-}" ] && echo "Environment='NO_PROXY=$no_proxy'" >> $proxy_env_conf
    systemctl daemon-reload
    systemctl restart docker
fi

if [ -z "${AUTOPKGTEST_TEST_UNINSTALLED-}" ]; then
    export AUTOPKGTEST_TEST_INSTALLED=yes
    build_docker=autopkgtest-build-docker
else
    build_docker=tools/autopkgtest-build-docker
fi

if ! "$build_docker" --vendor="$vendor" --release="$suite"; then
    echo "SKIP: Unable to build autopkgtest/$vendor:$suite container"
    exit 77
fi

export AUTOPKGTEST_TEST_DOCKER="autopkgtest/$vendor:$suite"
export PYTHONUNBUFFERED=1
# Wrapping the test in annotate-output helps to distinguish the output of
# the autopkgtest that is running these tests from the output of the
# autopkgtest that is under test.
exec annotate-output ./tests/autopkgtest DockerRunner
